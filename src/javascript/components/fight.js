import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper'

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const healthContainer = document.getElementsByClassName('arena___health-bar');
    const healthBars = [ ...healthContainer ];
    const statusContainer = document.getElementsByClassName('arena___health-indicator');
    const statusViews = [ ...statusContainer ];
    const statusInfo = {
      block: false,  
      currentHealth: 100,
      timeOfCrit: Date.now(),
      critInput: []
    }

    const playerOne = { 
      ...firstFighter, 
      ...statusInfo, 
      healthBar: healthBars[0], 
      statusView: statusViews[0],
      position: 'left'
    }

    const playerTwo = { 
      ...secondFighter, 
      ...statusInfo, 
      healthBar: healthBars[1], 
      statusView: statusViews[1],
      position: 'right'
    }

    function showStatus(fighter, text) {
      if(document.getElementById(`${fighter.position}-status-marker`)) {
        document.getElementById(`${fighter.position}-status-marker`).remove();
      }

    const statusMarker = createElement({ 
      tagName: 'div', 
      className: 'arena___status-marker', 
      attributes: {
        id: `${fighter.position}-status-marker`} });
      statusMarker.innerText = text;
      statusMarker.style.opacity = '1';
      fighter.statusView.append(statusMarker);
      setInterval(() => {
        if(statusMarker.style.opacity > 0) {
          statusMarker.style.opacity = statusMarker.style.opacity - 0.01;
        } else {
          statusMarker.remove();
        }
      }, 20);
    }

    function attackRelease(attacker, defender) {
      
      const totalDamage = getDamage(attacker, defender);

      if(!totalDamage) {
        showStatus(attacker, 'Missed!');
        return void 0;
      }

      if(attacker.critInput.length === 3) {
        showStatus(attacker, 'Critical hit!');
      }

      showStatus(defender, `-${totalDamage.toFixed(1)}`);
      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      if(defender.currentHealth <= 0) {
        document.removeEventListener('keydown', onDown);
        document.removeEventListener('keyup', onUp);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    function critHandler(fighter) {
      const currentTime = Date.now();

      if(currentTime - fighter.timeOfCrit < 10000) {
        return false;
      }

      if(!fighter.critInput.includes(event.code)) {
        fighter.critInput.push(event.code);
      }

      if(fighter.critInput.length === 3) {
        fighter.timeOfCrit = currentTime;
        return true;
      }
    }

    function onDown(e) {
      if(!e.repeat) {
        switch(e.code) {
          case controls.PlayerOneAttack: {
            attackRelease(playerOne, playerTwo);
            break;
          }

          case controls.PlayerTwoAttack: {
            attackRelease(playerTwo, playerOne);
            break;
          }

          case controls.PlayerOneBlock: {
            playerOne.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }

        if(controls.PlayerOneCriticalHitCombination.includes(e.code)) {
          critHandler(playerOne) ? attackRelease(playerOne, playerTwo) : null;
        }

        if(controls.PlayerTwoCriticalHitCombination.includes(e.code)) {
          critHandler(playerTwo) ? attackRelease(playerTwo, playerOne) : null;
        }
      }
    }

    function onUp(e) {
      switch(e.code) {
        case controls.PlayerOneBlock: 
          playerOne.block = false; 
          break;
        case controls.PlayerTwoBlock: 
          playerTwo.block = false; 
          break;
      }

      if(playerOne.critInput.includes(e.code)) {
        playerOne.critInput.splice(playerOne.critInput.indexOf(e.code), 1);
      }

      if(playerTwo.critInput.includes(e.code)) {
        playerTwo.critInput.splice(playerTwo.critInput.indexOf(e.code), 1);
      }
    }

    document.addEventListener('keydown', onDown);
    document.addEventListener('keyup', onUp);
  });
}

export function getDamage(attacker = 0, defender = 0) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  return fighter.isBlock ? 0 : fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return !fighter.isBlock ? 0:  defense * dodgeChance;
  
}

export function getSuperFight(fighter) {
  const { attack } = fighter;
  return 2 * attack;
}
